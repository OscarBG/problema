package Problema;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Problema {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		sc.nextLine();
		for (int k = 0; k < casos; k++) {
			String p = sc.nextLine();
			String p2 = sc.nextLine();
			String[] cosa = comprobar(p, p2);
			for (int i = 0; i < cosa.length; i++) {
				System.out.println(cosa[i]);
			}
		}
	}

	public static String[] comprobar(String p, String p2) {
		String[] pal = p.split(" "); 
		String[] pal2 = p2.split(" "); 
		HashSet<String> set = new HashSet<String>();
		for (int i = 0; i < pal.length; i++) {
			for (int j = 0; j < pal2.length; j++) {
				if (pal[i].equals(pal2[j])) {
					set.add(pal[i]);
				}
			}
		}
		String[] pal3 = new String[set.size()];
		set.toArray(pal3);
		return pal3;

	}
}
