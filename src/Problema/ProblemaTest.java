package Problema;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

class ProblemaTest {

	@Test
	public void testEasy() {
		String[] s1 = { "cd" };
		assertArrayEquals(s1, Problema.comprobar("md fc find more cd", "mkdir dif grep less cd"));
		String[] s2 = { "0" };
		assertArrayEquals(s2, Problema.comprobar("0 0 0 0 0", "0 0 0 0 0"));
		String[] s3 = {};
		assertArrayEquals(s3, Problema.comprobar("copy move dir cls del", "cp mv ls clear rm"));
		String[] s4 = { "325" };
		assertArrayEquals(s4, Problema.comprobar("12 5248 21256 325 2154", "123 45678 4584 325 1235"));
		String[] s5 = {};
		assertArrayEquals(s5, Problema.comprobar("456789 456789 12 45 78", "135 5478 4569 36987 12356"));
		String[] s6 = { "goretti" };
		assertArrayEquals(s6, Problema.comprobar("tecla lautaro goretti donato magali", "aurea lidon uxia goretti aloyses"));

	}

	@Test
	public void testNotEasy() {
		String[] s7 = { "UWU","IWI" };
		assertArrayEquals(s7, Problema.comprobar("OWO UWU EWE AWA IWI", "owo UWU ewe awa IWI"));
		String[] s8 = { "LAL","LOL" };
		assertArrayEquals(s8, Problema.comprobar("LUL LOL LEL LAL LIL", "lul LOL lel lil LAL"));
	}
}
